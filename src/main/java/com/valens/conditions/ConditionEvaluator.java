package com.valens.conditions;

public interface ConditionEvaluator
{
    public boolean evaluate(ConditionType type);
}
