/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author IHutuleac
 */
public class VisualCommit implements Serializable
{

    static final long serialVersionUID = -751118657453010399L;
    private String author;
    private String username;
    private String date;
    private Date timestamp;
    private String commitHash;
    private String shortHash;
    private String message;
    private String repository;
    private String project;

    private void readObject(ObjectInputStream input)
            throws IOException, ClassNotFoundException
    {
        // deserialize the non-transient data members first;
        input.defaultReadObject();

        project = (String) input.readObject();
        author = (String) input.readObject();
        username = (String) input.readObject();
        date = (String) input.readObject();
        timestamp = (Date) input.readObject();
        commitHash = (String) input.readObject();
        shortHash = (String) input.readObject();
        message = (String) input.readObject();
        repository = (String) input.readObject();
    }

    private void writeObject(ObjectOutputStream output)
            throws IOException, ClassNotFoundException
    {
        // serialize the non-transient data members first;
        output.defaultWriteObject();
        output.writeObject(project);
        output.writeObject(author);
        output.writeObject(username);
        output.writeObject(date);
        output.writeObject(timestamp);
        output.writeObject(commitHash);
        output.writeObject(shortHash);
        output.writeObject(message);
        output.writeObject(repository);
    }

    public VisualCommit(String author, String username, String date, Date timestamp, String commitHash, String shortHash, String message, String repository, String project)
    {
        this.author = author;
        this.username = username;
        this.date = date;
        this.timestamp = timestamp;
        this.commitHash = commitHash;
        this.shortHash = shortHash;
        this.message = message;
        this.repository = repository;
        this.project = project;
    }

    public String getRepository()
    {
        return repository;
    }

    public void setRepository(String repository)
    {
        this.repository = repository;
    }

    public String getProject()
    {
        return project;
    }

    public void setProject(String project)
    {
        this.project = project;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getCommitHash()
    {
        return commitHash;
    }

    public void setCommitHash(String commitHash)
    {
        this.commitHash = commitHash;
    }

    public String getShortHash()
    {
        return shortHash;
    }

    public void setShortHash(String shortHash)
    {
        this.shortHash = shortHash;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Date getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Date timestamp)
    {
        this.timestamp = timestamp;
    }
}
