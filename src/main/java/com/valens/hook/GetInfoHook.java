/*
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.hook;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest.Builder;
import com.atlassian.bitbucket.hook.repository.PostRepositoryHook;
import com.atlassian.bitbucket.hook.repository.PostRepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryPushHookRequest;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.valens.CommitArchiveService;

import java.util.List;
import javax.annotation.Nonnull;


public class GetInfoHook
        implements PostRepositoryHook<RepositoryPushHookRequest>, SettingsValidator {
    private final CommitService commitService;
    private final NavBuilder navBuilder;

    public GetInfoHook(CommitService commitService, NavBuilder navBuilder) {
        this.commitService = commitService;
        this.navBuilder = navBuilder;
    }

    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors settingsValidationErrors, @Nonnull Scope scope) {
        if (settings.getString("url", "").isEmpty()) {
            settingsValidationErrors.addFieldError("url", "Url field is blank, please supply one");
        }
    }

    @Override
    public void postUpdate(@Nonnull PostRepositoryHookContext postRepositoryHookContext, @Nonnull RepositoryPushHookRequest repositoryPushHookRequest) {
        for (RefChange refChange : repositoryPushHookRequest.getRefChanges()) {
            CommitsBetweenRequest.Builder commitsBetweenBuilder = new CommitsBetweenRequest.Builder(repositoryPushHookRequest.getRepository());
            commitsBetweenBuilder.exclude(refChange.getFromHash(), new String[0]);
            commitsBetweenBuilder.include(refChange.getToHash(), new String[0]);

            PageRequest pageRequest = new PageRequestImpl(0, 6);

            Page<Commit> commits = commitService.getCommitsBetween(commitsBetweenBuilder.build(), pageRequest);
            String key = repositoryPushHookRequest.getRepository().getProject().getKey();

            for (Commit commit : commits.getValues()) {
                if (CommitArchiveService.getInstance().getList().size() > 1000)
                    CommitArchiveService.getInstance().getList().remove(0);
                CommitArchiveService.getInstance().getList().add(commit);
            }
        }
    }
}
