/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens;

import com.atlassian.bitbucket.commit.Commit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author IHutuleac
 */
public class CommitArchiveService implements Serializable
{
    static final long serialVersionUID = -7511180123693010399L;
    
    List<Commit> list = Collections.synchronizedList(new ArrayList<Commit>());

    public List<Commit> getList()
    {
        return list;
    }

    public void setList(List<Commit> list)
    {
        this.list = list;
    }
            
    private CommitArchiveService()
    {
    }
    
    public void save()
    {
        ObjectOutputStream oos = null;
        try
        {
            oos = new ObjectOutputStream(
                    new FileOutputStream(new File("../temp/.commits.archive.dat")));

            oos.writeObject(list);

            oos.close();
        } catch (IOException ex)
        {
            try
            {
                if (oos != null)
                {
                    oos.close();
                }
            } catch (IOException ex1)
            {

            }
            Logger.getLogger(CommitArchiveService.class.getName()).log(Level.WARNING, "Cannot save history");
        }
    }
    
    public static CommitArchiveService getInstance()
    {
        if(CommitArchiveServiceHolder.INSTANCE == null){
            CommitArchiveServiceHolder.INSTANCE = new CommitArchiveService();
            ObjectInputStream oos ;
            try
            {
                oos = new ObjectInputStream(                                 
                        new FileInputStream(  new File("../temp/.commits.archive.dat")) );
                CommitArchiveServiceHolder.INSTANCE.list = (List<Commit>) oos.readObject();
            } catch (Exception ex) {                   
                CommitArchiveServiceHolder.INSTANCE = new CommitArchiveService();
            }          
            
        }
        return CommitArchiveServiceHolder.INSTANCE;
    }
    
    private static class CommitArchiveServiceHolder
    {
        private static CommitArchiveService INSTANCE = null;  
        
    }
}
