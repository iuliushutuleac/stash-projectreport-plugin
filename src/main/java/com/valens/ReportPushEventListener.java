/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens;

import com.atlassian.event.api.EventListener;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.event.repository.RepositoryPushEvent;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author IHutuleac
 */
public class ReportPushEventListener
{

    private final CommitService commitService;
    private final NavBuilder navBuilder;

    public ReportPushEventListener(CommitService commitService, NavBuilder navBuilder)
    {
        this.commitService = commitService;
        this.navBuilder = navBuilder;
    }

    @EventListener
    public void mylistener(RepositoryPushEvent pushEvent)
    {
        synchronized (commitService)
        {

            for (RefChange refChange : pushEvent.getRefChanges())
            {
                CommitsBetweenRequest.Builder commitsBetweenBuilder = new CommitsBetweenRequest.Builder(pushEvent.getRepository());
                commitsBetweenBuilder.exclude(refChange.getFromHash()); //Starting with
                commitsBetweenBuilder.include(refChange.getToHash()); // ending with

                PageRequest pageRequest = new PageRequestImpl(0, 6);

                Page<Commit> commits = commitService.getCommitsBetween(commitsBetweenBuilder.build(), pageRequest);
                String key = pushEvent.getRepository().getProject().getKey();
                //TODO handle Pages
                for (Commit commit : commits.getValues())
                {
                    if (CommitArchiveService.getInstance().getList().size() > 1000)
                    {
                        CommitArchiveService.getInstance().getList().remove(0);
                    }
                    CommitArchiveService.getInstance().getList().add(commit);

                }

                CommitArchiveService.getInstance().save();
            }
        }
    }
}
