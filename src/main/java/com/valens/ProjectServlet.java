/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens;

import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.project.ProjectService;

import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.DetailedUser;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.user.UserAdminService;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.google.common.collect.ImmutableMap;
import com.valens.conditions.ConditionEvaluator;
import com.valens.conditions.ConditionEvaluatorImpl;
import com.valens.conditions.ConditionType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ProjectServlet extends AbstractServlet
{

    private final ProjectService projectService;
    private final RepositoryService repositoryService;
    private final SecurityService securityService;
    private final CommitService commitService;
    private final UserAdminService userAdminService;
    private final PluginLicenseManager licenseManager;

    public ProjectServlet(SoyTemplateRenderer soyTemplateRenderer, ProjectService projectService,
            RepositoryService repositoryService,
            SecurityService securityService,
            CommitService commitService,
            UserAdminService userAdminService,
                          PluginLicenseManager licenseManager)
    {
        super(soyTemplateRenderer);
        this.projectService = projectService;
        this.repositoryService = repositoryService;
        this.securityService = securityService;
        this.commitService = commitService;
        this.userAdminService = userAdminService;
        this.licenseManager = licenseManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
//
//
//        ConditionEvaluator eval = new ConditionEvaluatorImpl(licenseManager);
//        if (!eval.evaluate(ConditionType.LICENSED)) {
//            resp.setContentType("text/html;charset=utf-8");
//            resp.getWriter().append("Your license for History Report is invalid or expired !");
//            return;
//        }

        // Get projectKey from path
        String pathInfo = req.getPathInfo();
        String contextPath = req.getContextPath();

        String[] components = pathInfo.split("/");

        if (components.length < 2)
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        Project project = projectService.getByKey(components[1]);

        if (project == null)
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        boolean isSettings = false;
        if (components.length == 3 && "settings".equalsIgnoreCase(components[2]))
        {
            isSettings = true;
        }

        ArrayList<VisualCommit> commits = new ArrayList<VisualCommit>();
        for (Commit aux : CommitArchiveService.getInstance().getList())
        {
            if (aux.getRepository().getProject().getKey().equalsIgnoreCase(project.getKey()))
            {
                commits.add(new VisualCommit(
                        aux.getAuthor() instanceof ApplicationUser ? ((ApplicationUser) aux.getAuthor()).getDisplayName() : aux.getAuthor().getName(),
                        aux.getAuthor().getName(),
                        aux.getAuthorTimestamp().toGMTString(),
                        aux.getAuthorTimestamp(),
                        aux.getId(),
                        aux.getDisplayId(),
                        aux.getMessage(),
                        aux.getRepository().getName(),
                        aux.getRepository().getProject().getKey()
                        ));
            }
        }

        Collections.sort(commits, new Comparator<VisualCommit>()
        {
            @Override
            public int compare(VisualCommit fruite1, VisualCommit fruite2)
            {

                return fruite2.getTimestamp().compareTo(fruite1.getTimestamp());
            }
        });

        String template = "plugin.project";

        render(resp, template, ImmutableMap.<String, Object>of("project", project,
                "commits", commits, "contextPath", contextPath));
    }

}
