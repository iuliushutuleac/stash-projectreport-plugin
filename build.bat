set M2HOME=C:\Applications\Atlassian\atlassian-plugin-sdk-6.2.14\apache-maven-3.2.1
set PATH=%M2HOME%\bin;%PATH%

cmd /c mvn build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.nextIncrementalVersion}

cmd /c mvn -Dbamboo.version=5.14.3.1 clean package

REM git add .

REM git commit -m "new version"

REM git push

pause