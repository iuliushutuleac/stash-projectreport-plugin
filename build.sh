export M2HOME=/work/Applications/Atlassian/atlassian-plugin-sdk-6.2.14/apache-maven-3.2.1/
export PATH=$M2HOME/bin:$PATH
export ATLAS_HOME=/work/Applications/Atlassian/atlassian-plugin-sdk-6.2.14

mvn build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.nextIncrementalVersion}

mvn -Dbitbucket.version=5.3.0 clean package
